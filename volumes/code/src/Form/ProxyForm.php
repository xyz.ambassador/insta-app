<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;

class ProxyForm extends AbstractType
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $port;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @return string
     */
    public function getHost(): ?string
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host): void
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getPort(): ?string
    {
        return $this->port;
    }

    /**
     * @param string $port
     */
    public function setPort(string $port): void
    {
        $this->port = $port;
    }

    /**
     * @return string
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return ProxyGroup
     */
    public function getProxyGroup(): ?ProxyGroup
    {
        return $this->proxyGroup;
    }

    /**
     * @param ProxyGroup $proxyGroup
     */
    public function setProxyGroup(ProxyGroup $proxyGroup): void
    {
        $this->proxyGroup = $proxyGroup;
    }
}
