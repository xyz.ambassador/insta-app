<?php

namespace App\Controller\Instagram;

use App\Entity\Instagram\Account;
use App\Form\Instagram\AccountType;
use App\Manager\Instagram\AccountManager;
use App\Repository\Instagram\AccountRepository;
use App\Security\Voter\Instagram\AccountVoter;
use Rares\ModalBundle\Response\ModalRedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AccountController
 *
 * @package App\Controller\Instagram
 */
class AccountController extends AbstractController
{
    /**
     * @param AccountRepository $accountRepository
     *
     * @return Response
     */
    public function index(AccountRepository $accountRepository): Response
    {
        return $this->render('instagram/account/index.html.twig', [
            'accounts' => $accountRepository->findBy([
                'user' => $this->getUser(),
            ]),
        ]);
    }

    /**
     * @param AccountManager $manager
     * @param Request        $request
     *
     * @return Response
     */
    public function new(AccountManager $manager, Request $request): Response
    {
        $form = $this->createForm(AccountType::class, null, [
            'action' => $this->generateUrl('instagram.account.new'),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->create($this->getUser(), $form->getData());

            $this->addFlash('notice', 'Account added.');

            return new ModalRedirectResponse(
                $this->generateUrl('instagram.account')
            );
        }

        return $this->render('instagram/account/new.html.twig', [
           'form' => $form->createView(),
        ]);
    }

    /**
     * @param Account $account
     *
     * @return Response
     */
    public function show(Account $account): Response
    {
        $this->denyAccessUnlessGranted(AccountVoter::SHOW, $account);

        return $this->render('instagram/account/show.html.twig', ['account' => $account]);
    }

    /**
     * @param AccountManager $manager
     * @param Account        $account
     * @param Request        $request
     *
     * @return Response
     */
    public function edit(AccountManager $manager, Account $account, Request $request): Response
    {
        $this->denyAccessUnlessGranted(AccountVoter::EDIT, $account);

        $form = $this->createForm(AccountType::class, $manager->getForm($account), [
            'action' => $this->generateUrl(
                'instagram.account.edit',
                ['id' => $account->getId()]
            ),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->update($account, $form->getData());

            $this->addFlash('notice', 'Account edited.');

            return new ModalRedirectResponse(
                $this->generateUrl('instagram.account')
            );
        }

        return $this->render('instagram/account/edit.html.twig', [
            'account' => $account,
            'form'    => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Account $account
     *
     * @return Response
     */
    public function delete(Request $request, Account $account): Response
    {
        $this->denyAccessUnlessGranted(AccountVoter::EDIT, $account);

        if ($this->isCsrfTokenValid('delete'.$account->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($account);
            $em->flush();
        }

        return $this->redirectToRoute('instagram.account');
    }
}
