<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Timing", mappedBy="user", orphanRemoval=true)
     */
    private $timings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Proxy", mappedBy="user", orphanRemoval=true)
     */
    private $proxies;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\File", mappedBy="user", orphanRemoval=true)
     */
    private $files;

    public function __construct()
    {
        parent::__construct();
        $this->timings = new ArrayCollection();
        $this->proxies = new ArrayCollection();
        $this->files = new ArrayCollection();
    }


    public function addTiming(Timing $timing): self
    {
        if (!$this->timings->contains($timing)) {
            $this->timings[] = $timing;
            $timing->setUser($this);
        }

        return $this;
    }

    public function removeTiming(Timing $timing): self
    {
        if ($this->timings->contains($timing)) {
            $this->timings->removeElement($timing);
            // set the owning side to null (unless already changed)
            if ($timing->getUser() === $this) {
                $timing->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Proxy[]
     */
    public function getProxies(): Collection
    {
        return $this->proxies;
    }

    public function addProxy(Proxy $proxy): self
    {
        if (!$this->proxies->contains($proxy)) {
            $this->proxies[] = $proxy;
            $proxy->setUser($this);
        }

        return $this;
    }

    public function removeProxy(Proxy $proxy): self
    {
        if ($this->proxies->contains($proxy)) {
            $this->proxies->removeElement($proxy);
            // set the owning side to null (unless already changed)
            if ($proxy->getUser() === $this) {
                $proxy->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setUser($this);
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
            // set the owning side to null (unless already changed)
            if ($file->getUser() === $this) {
                $file->setUser(null);
            }
        }

        return $this;
    }
}