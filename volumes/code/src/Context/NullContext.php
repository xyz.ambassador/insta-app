<?php declare(strict_types = 1);

namespace App\Context;

use App\Context\Declaration\ContextInterface;
use App\Entity\Instagram\Account;
use Symfony\Component\Console\Logger\ConsoleLogger;

/**
 * Class MockContext
 * @package App\Context
 */
class NullContext implements ContextInterface
{
    /**
     * @return Account
     */
    public function getAccount(): ?Account
    {
        return null;
    }

    /**
     * @param Account $account
     *
     * @return ContextInterface
     */
    public function setAccount(Account $account): ContextInterface
    {
        return $this;
    }

    /**
     * @return null|ConsoleLogger
     */
    public function getConsoleLogger(): ?ConsoleLogger
    {
        return null;
    }

    /**
     * @param ConsoleLogger $logger
     * @return ContextInterface
     */
    public function setConsoleLogger(ConsoleLogger $logger): ContextInterface
    {
       return $this;
    }

    /**
     * @return null|string
     */
    public function getConsoleCommand(): ?string
    {
        return null;
    }

    /**
     * @param string $command
     * @return ContextInterface
     */
    public function setConsoleCommand(string $command): ContextInterface
    {
        return null;
    }

    /**
     * @return ContextInterface
     */
    public function clear(): ContextInterface
    {
        return $this;
    }
}
