<?php

namespace App\Security\Voter\Instagram;

use App\Entity\Instagram\Account;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class AccountVoter
 *
 * @package App\Security\Voter\Instagram
 */
class AccountVoter extends Voter
{
    const SHOW = 'show';

    const EDIT = 'edit';

    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        return $subject instanceof Account;
    }

    /**
     * @param string         $attribute
     * @param Account        $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        return $subject->getUser() == $user;
    }
}
