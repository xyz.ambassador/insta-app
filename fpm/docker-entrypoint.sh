#!/bin/bash
set -e

#test -d /var/www/app/var/logs/app || mkdir /var/www/app/var/logs/app
#
#if [ -d "/var/www/app/app/config" ]; then
#    cp /usr/local/share/parameters.yml /var/www/app/app/config/parameters.yml
#    cp /usr/local/share/.env /var/www/app/.env
#    chown -R www-data:www-data /var/www/app
#fi

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

exec "$@"
