<?php declare(strict_types = 1);

namespace App\Event\Instagram;

use App\Entity\Instagram\Account;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class AccountEvent
 *
 * @package App\Event\Instagram
 */
class AccountEvent extends Event
{
    const AFTER_CREATE = 'app.instagram.account.afterCreate';

    /**
     * @var Account
     */
    private $entity;

    /**
     * AccountEvent constructor.
     *
     * @param Account $entity
     */
    public function __construct(Account $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return Account
     */
    public function getEntity(): Account
    {
        return $this->entity;
    }
}
