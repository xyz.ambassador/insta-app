<?php

namespace App\Form\Instagram;

use App\Entity\Proxy;

/**
 * Class AccountForm
 *
 * @package App\Form\Instagram
 */
class AccountForm
{
    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var Proxy
     */
    private $proxy;

    /**
     * @return string
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param string $login
     *
     * @return AccountForm
     */
    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return AccountForm
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Proxy
     */
    public function getProxy(): ?Proxy
    {
        return $this->proxy;
    }

    /**
     * @param Proxy $proxy
     *
     * @return AccountForm
     */
    public function setProxy(Proxy $proxy): self
    {
        $this->proxy = $proxy;

        return $this;
    }
}
