<?php

namespace App\Repository\Instagram;

use App\Entity\Instagram\AccountTiming;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AccountTiming|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountTiming|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountTiming[]    findAll()
 * @method AccountTiming[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountTimingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AccountTiming::class);
    }

    // /**
    //  * @return AccountTiming[] Returns an array of AccountTiming objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AccountTiming
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
