<?php

namespace App\Entity;

use App\Entity\Instagram\AccountTiming;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TimingRepository")
 */
class Timing
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="timings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $value;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Instagram\AccountTiming", mappedBy="timing")
     */
    private $accountTimings;

    public function __construct()
    {
        $this->accountTimings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Collection|AccountTiming[]
     */
    public function getAccountTimings(): Collection
    {
        return $this->accountTimings;
    }

    public function addAccountTiming(AccountTiming $accountTiming): self
    {
        if (!$this->accountTimings->contains($accountTiming)) {
            $this->accountTimings[] = $accountTiming;
            $accountTiming->setTiming($this);
        }

        return $this;
    }

    public function removeAccountTiming(AccountTiming $accountTiming): self
    {
        if ($this->accountTimings->contains($accountTiming)) {
            $this->accountTimings->removeElement($accountTiming);
            // set the owning side to null (unless already changed)
            if ($accountTiming->getTiming() === $this) {
                $accountTiming->setTiming(null);
            }
        }

        return $this;
    }
}
