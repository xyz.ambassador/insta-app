<?php

namespace App\Entity\Instagram;

use App\Entity\Timing;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Instagram\AccountTimingRepository")
 * @ORM\Table(name="instagram_account_timing")
 */
class AccountTiming
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Instagram\Account", inversedBy="accountTimings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Timing", inversedBy="accountTimings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $timing;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getTiming(): ?Timing
    {
        return $this->timing;
    }

    public function setTiming(?Timing $timing): self
    {
        $this->timing = $timing;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }
}
