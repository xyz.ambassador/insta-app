<?php declare(strict_types = 1);

namespace App\Context;

use App\Entity\Instagram\Account;

/**
 * Class InstagramContext
 *
 * @package App\Context
 */
class InstagramContext
{
    /**
     * @var Account
     */
    private $account;

    /**
     * @return Account
     */
    public function getAccount(): ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount(Account $account): void
    {
        $this->account = $account;
    }

    /**
     * @return self
     */
    public function clear(): self
    {
        return $this;
    }
}
