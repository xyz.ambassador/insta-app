<?php

namespace App\Controller;

use App\Entity\Proxy;
use App\Form\ProxyType;
use App\Manager\ProxyManager;
use App\Repository\ProxyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProxyController
 *
 * @package App\Controller
 */
class ProxyController extends AbstractController
{
    /**
     * @param ProxyRepository $proxyRepository
     *
     * @return Response
     */
    public function index(ProxyRepository $proxyRepository): Response
    {
        return $this->render('proxy/index.html.twig', ['proxies' => $proxyRepository->findAll()]);
    }

    /**
     * @param ProxyManager $manager
     * @param Request      $request
     *
     * @return Response
     */
    public function new(ProxyManager $manager, Request $request): Response
    {
        $form = $this->createForm(ProxyType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->create($this->getUser(), $form->getData());

            return $this->redirectToRoute('app.proxy');
        }

        return $this->render('proxy/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Proxy $proxy
     *
     * @return Response
     */
    public function show(Proxy $proxy): Response
    {
        return $this->render('proxy/show.html.twig', ['proxy' => $proxy]);
    }

    /**
     * @param Request $request
     * @param Proxy   $proxy
     *
     * @return Response
     */
    public function edit(Request $request, Proxy $proxy): Response
    {
        $form = $this->createForm(ProxyType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('app.proxy', ['id' => $proxy->getId()]);
        }

        return $this->render('proxy/edit.html.twig', [
            'proxy' => $proxy,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Proxy   $proxy
     *
     * @return Response
     */
    public function delete(Request $request, Proxy $proxy): Response
    {
        if ($this->isCsrfTokenValid('delete'.$proxy->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($proxy);
            $em->flush();
        }

        return $this->redirectToRoute('app.proxy');
    }
}
