<?php

namespace App\Manager\Instagram;

use App\Context\ContextAwareTrait;
use App\Context\Declaration\ContextAwareInterface;
use App\Context\NullContext;
use App\Entity\Instagram\Account;
use App\Entity\User;
use App\Event\Instagram\AccountEvent;
use App\Form\Instagram\AccountForm;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use App\Repository\Instagram\AccountRepository;
use Psr\Log\NullLogger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class AccountManager
 *
 * @package App\Manager\Instagram
 */
class AccountManager implements LoggerAwareInterface, ContextAwareInterface
{
    use LoggerAwareTrait;
    use ContextAwareTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var AccountRepository
     */
    private $repository;

    /**
     * AccountManager constructor.
     *
     * @param EntityManagerInterface   $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->repository = $em->getRepository(Account::class);

        $this->setLogger(new NullLogger());
        $this->setContext(new NullContext());
    }

    /**
     * @return AccountRepository
     */
    public function getRepository(): AccountRepository
    {
        return $this->repository;
    }

    /**
     * @param Account $account
     */
    public function save(Account $account): void
    {
        $this->context->setAccount($account);

        $this->em->persist($account);
        $this->em->flush();
    }

    /**
     * @param Account $account
     *
     * @return AccountForm
     */
    public function getForm(Account $account): AccountForm
    {
        $this->context->setAccount($account);

        $form = (new AccountForm())
            ->setLogin($account->getLogin())
            ->setPassword($account->getPassword())
            ->setProxy($account->getProxy())
        ;

        return $form;
    }

    /**
     * @param Account     $account
     * @param AccountForm $form
     */
    public function update(Account $account, AccountForm $form): void
    {
        $this->context->setAccount($account);

        $account
            ->setLogin($form->getLogin())
            ->setPassword($form->getPassword())
            ->setProxy($form->getProxy())
        ;

        $this->save($account);
    }

    /**
     * @param User        $user
     * @param AccountForm $form
     *
     * @return Account
     */
    public function create(User $user, AccountForm $form): Account
    {
        $account = (new Account())->setUser($user);

        $this->update($account, $form);

        $this->dispatcher->dispatch(
            AccountEvent::AFTER_CREATE,
            new AccountEvent($account)
        );

        return $account;
    }
}
