<?php declare(strict_types = 1);

namespace App\Context;

use App\Context\Declaration\ContextInterface;

/**
 * Trait ContextAwareTrait
 * @package App\Context
 */
trait ContextAwareTrait
{
    /**
     * The context instance.
     *
     * @var ContextInterface
     */
    protected $context;

    /**
     * Sets a context.
     *
     * @param ContextInterface $context
     */
    public function setContext(ContextInterface $context): void
    {
        $this->context = $context;
    }
}
