<?php declare(strict_types = 1);

namespace App\Event;

use App\Entity\Proxy;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ProxyEvent
 *
 * @package App\Event
 */
class ProxyEvent extends Event
{
    const AFTER_CREATE = 'app.proxy.afterCreate';

    /**
     * @var Proxy
     */
    private $entity;

    /**
     * ProxyEvent constructor.
     *
     * @param Proxy $entity
     */
    public function __construct(Proxy $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return Proxy
     */
    public function getEntity(): Proxy
    {
        return $this->entity;
    }
}
