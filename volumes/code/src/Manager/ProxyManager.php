<?php

namespace App\Manager;

use App\Context\ContextAwareTrait;
use App\Context\Declaration\ContextAwareInterface;
use App\Context\NullContext;
use App\Entity\Proxy;
use App\Entity\User;
use App\Event\ProxyEvent;
use App\Form\ProxyForm;
use App\Repository\ProxyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ProxyManager
 *
 * @package App\Manager
 */
class ProxyManager implements LoggerAwareInterface, ContextAwareInterface
{
    use LoggerAwareTrait;
    use ContextAwareTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var ProxyRepository
     */
    private $repository;

    /**
     * ProxyManager constructor.
     *
     * @param EntityManagerInterface   $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->repository = $em->getRepository(Proxy::class);

        $this->setLogger(new NullLogger());
        $this->setContext(new NullContext());
    }

    /**
     * @return ProxyRepository
     */
    public function getRepository(): ProxyRepository
    {
        return $this->repository;
    }

    /**
     * @param Proxy $proxy
     */
    public function save(Proxy $proxy): void
    {
        $this->em->persist($proxy);
        $this->em->flush();
    }

    /**
     * @param User      $user
     * @param ProxyForm $form
     *
     * @return Proxy
     */
    public function create(User $user, ProxyForm $form): Proxy
    {
        $proxy = (new Proxy())
            ->setUser($user)
            ->setHost($form->getHost())
            ->setPort($form->getPort())
            ->setLogin($form->getLogin())
            ->setPassword($form->getPassword())
        ;

        $this->save($proxy);

        $this->dispatcher->dispatch(
            ProxyEvent::AFTER_CREATE,
            new ProxyEvent($proxy)
        );

        return $proxy;
    }
}
