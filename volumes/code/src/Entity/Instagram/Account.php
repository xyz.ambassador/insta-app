<?php

namespace App\Entity\Instagram;

use App\Entity\Proxy;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="instagram_account")
 * @ORM\Entity(repositoryClass="App\Repository\Instagram\AccountRepository")
 * @Gedmo\Loggable
 */
class Account
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    const STATE_UNAUTHORIZED = 0;

    const STATE_ACTIVE = 1;

    const STATE_INVALID_CREDENTIALS = 2;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var Proxy
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Proxy")
     */
    private $proxy;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint")
     */
    private $state = self::STATE_UNAUTHORIZED;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Instagram\AccountTiming", mappedBy="account", orphanRemoval=true)
     */
    private $accountTimings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Instagram\UploadFeed", mappedBy="account", orphanRemoval=true)
     */
    private $uploadFeeds;

    /**
     * Account constructor.
     */
    public function __construct()
    {
        $this->accountTimings = new ArrayCollection();
        $this->uploadFeeds = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     *
     * @return Account
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Proxy|null
     */
    public function getProxy(): ?Proxy
    {
        return $this->proxy;
    }

    /**
     * @param Proxy|null $proxy
     *
     * @return Account
     */
    public function setProxy(?Proxy $proxy): self
    {
        $this->proxy = $proxy;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param string $login
     *
     * @return Account
     */
    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return Account
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getState(): ?int
    {
        return $this->state;
    }

    /**
     * @param int $state
     *
     * @return Account
     */
    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection|AccountTiming[]
     */
    public function getAccountTimings(): Collection
    {
        return $this->accountTimings;
    }

    /**
     * @param AccountTiming $accountTiming
     *
     * @return Account
     */
    public function addAccountTiming(AccountTiming $accountTiming): self
    {
        if (!$this->accountTimings->contains($accountTiming)) {
            $this->accountTimings[] = $accountTiming;
            $accountTiming->setAccount($this);
        }

        return $this;
    }

    /**
     * @param AccountTiming $accountTiming
     *
     * @return Account
     */
    public function removeAccountTiming(AccountTiming $accountTiming): self
    {
        if ($this->accountTimings->contains($accountTiming)) {
            $this->accountTimings->removeElement($accountTiming);
            // set the owning side to null (unless already changed)
            if ($accountTiming->getAccount() === $this) {
                $accountTiming->setAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UploadFeed[]
     */
    public function getUploadFeeds(): Collection
    {
        return $this->uploadFeeds;
    }

    /**
     * @param UploadFeed $uploadFeed
     *
     * @return Account
     */
    public function addUploadFeed(UploadFeed $uploadFeed): self
    {
        if (!$this->uploadFeeds->contains($uploadFeed)) {
            $this->uploadFeeds[] = $uploadFeed;
            $uploadFeed->setAccount($this);
        }

        return $this;
    }

    /**
     * @param UploadFeed $uploadFeed
     *
     * @return Account
     */
    public function removeUploadFeed(UploadFeed $uploadFeed): self
    {
        if ($this->uploadFeeds->contains($uploadFeed)) {
            $this->uploadFeeds->removeElement($uploadFeed);
            // set the owning side to null (unless already changed)
            if ($uploadFeed->getAccount() === $this) {
                $uploadFeed->setAccount(null);
            }
        }

        return $this;
    }

//    public function isAllowUser
}
