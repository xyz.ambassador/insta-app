<?php

namespace App\Menu;

use App\Entity\User;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class MenuBuilder
 *
 * @package App\Menu
 */
class MenuBuilder
{
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var Security
     */
    private $security;

    /**
     * MenuBuilder constructor.
     *
     * @param FactoryInterface $factory
     * @param Security         $security
     */
    public function __construct(
        FactoryInterface $factory,
        Security $security
    ) {
        $this->factory = $factory;
        $this->security = $security;
    }

    /**
     * @param array $options
     *
     * @return ItemInterface
     */
    public function createLeftMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'navbar-nav mr-auto');

        if ($user = $this->security->getUser()) {
            $this->createLeftUserMenu($menu, $user);
        } else {
            $this->createLeftGuestMenu($menu);
        }

        foreach ($menu as $child) {
            $child->setLinkAttribute('class', 'nav-link')
                ->setAttribute('class', 'nav-item');
        }

        return $menu;
    }

    /**
     * @param array $options
     *
     * @return ItemInterface
     */
    public function createRightMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'navbar-nav');

        if ($user = $this->security->getUser()) {
            $this->createRightUserMenu($menu, $user);
        } else {
            $this->createRightGuestMenu($menu);
        }

        return $menu;
    }

    /**
     * @param ItemInterface $menu
     * @param User          $user
     */
    private function createLeftUserMenu(ItemInterface $menu, User $user): void
    {
        $menu->addChild('Accounts', ['route' => 'instagram.account']);
        $menu->addChild('Proxies', ['route' => 'app.proxy']);
    }

    /**
     * @param ItemInterface $menu
     */
    private function createLeftGuestMenu(ItemInterface $menu): void
    {
    }

    /**
     * @param ItemInterface $menu
     * @param User          $user
     */
    private function createRightUserMenu(ItemInterface $menu, User $user): void
    {
        $item = $menu->addChild($user->getUsername(), [
            'attributes' => [
                'class' => 'nav-item dropdown',
            ],
            'linkAttributes' => [
                'class' => 'nav-link dropdown-toggle',
                'role'  => 'button',
                'data-toggle' => 'dropdown',
            ],
            'uri' => '#',
        ]);

        $item->addChild('Profile', [
            'route' => 'fos_user_profile_show',
            'attributes' => [
                'id' => 'back_to_homepage'
            ],
        ]);

        $item->addChild('Logout', [
            'route' => 'fos_user_security_logout',
            'attributes' => [
                'divider_prepend' => true,
                'icon' => 'fa fa-sign-out',
            ],
        ]);
    }

    /**
     * @param ItemInterface $menu
     */
    private function createRightGuestMenu(ItemInterface $menu): void
    {
        $menu->addChild('Sign in', ['route' => 'fos_user_security_login']);
        $menu->addChild('Sign up', ['route' => 'fos_user_registration_register']);
    }
}