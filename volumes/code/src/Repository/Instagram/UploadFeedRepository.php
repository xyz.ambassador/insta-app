<?php

namespace App\Repository\Instagram;

use App\Entity\Instagram\UploadFeed;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UploadFeed|null find($id, $lockMode = null, $lockVersion = null)
 * @method UploadFeed|null findOneBy(array $criteria, array $orderBy = null)
 * @method UploadFeed[]    findAll()
 * @method UploadFeed[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UploadFeedRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UploadFeed::class);
    }

    // /**
    //  * @return UploadFeed[] Returns an array of UploadFeed objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UploadFeed
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
