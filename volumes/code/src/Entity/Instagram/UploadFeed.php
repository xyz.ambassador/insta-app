<?php

namespace App\Entity\Instagram;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Instagram\UploadFeedRepository")
 * @ORM\Table(name="instagram_upload_feed")
 */
class UploadFeed
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Instagram\Account", inversedBy="uploadFeeds")
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;

    /**
     * @ORM\Column(type="smallint")
     */
    private $state;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $caption;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $location;

    /**
     * @ORM\Column(type="datetime")
     */
    private $scheduledAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Instagram\UploadFeedFile", mappedBy="uploadFeed", orphanRemoval=true)
     */
    private $uploadFeedFiles;

    public function __construct()
    {
        $this->uploadFeedFiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getCaption(): ?string
    {
        return $this->caption;
    }

    public function setCaption(string $caption): self
    {
        $this->caption = $caption;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getScheduledAt(): ?\DateTimeInterface
    {
        return $this->scheduledAt;
    }

    public function setScheduledAt(\DateTimeInterface $scheduledAt): self
    {
        $this->scheduledAt = $scheduledAt;

        return $this;
    }

    /**
     * @return Collection|UploadFeedFile[]
     */
    public function getUploadFeedFiles(): Collection
    {
        return $this->uploadFeedFiles;
    }

    public function addUploadFeedFile(UploadFeedFile $uploadFeedFile): self
    {
        if (!$this->uploadFeedFiles->contains($uploadFeedFile)) {
            $this->uploadFeedFiles[] = $uploadFeedFile;
            $uploadFeedFile->setUploadFeed($this);
        }

        return $this;
    }

    public function removeUploadFeedFile(UploadFeedFile $uploadFeedFile): self
    {
        if ($this->uploadFeedFiles->contains($uploadFeedFile)) {
            $this->uploadFeedFiles->removeElement($uploadFeedFile);
            // set the owning side to null (unless already changed)
            if ($uploadFeedFile->getUploadFeed() === $this) {
                $uploadFeedFile->setUploadFeed(null);
            }
        }

        return $this;
    }
}
