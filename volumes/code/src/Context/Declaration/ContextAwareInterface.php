<?php declare(strict_types = 1);

namespace App\Context\Declaration;

/**
 * Interface ContextAwareInterface
 * @package App\Context\Declaration
 */
interface ContextAwareInterface
{
    /**
     * @param ContextInterface $context
     */
    public function setContext(ContextInterface $context): void;
}
