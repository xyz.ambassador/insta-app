<?php declare(strict_types = 1);

namespace App\Context;

use App\Context\Declaration\ContextInterface;

use App\Entity\Instagram\Account;
use Symfony\Component\Console\Logger\ConsoleLogger;

/**
 * Class AppContext
 * @package App\Context
 */
class AppContext implements ContextInterface
{
    /**
     * @var Account
     */
    private $account;

    /**
     * @var ConsoleLogger
     */
    private $consoleLogger;

    /**
     * @var string
     */
    private $consoleCommand;

    /**
     * @return Account
     */
    public function getAccount(): ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     *
     * @return ContextInterface
     */
    public function setAccount(Account $account): ContextInterface
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return null|ConsoleLogger
     */
    public function getConsoleLogger(): ?ConsoleLogger
    {
        return $this->consoleLogger;
    }

    /**
     * @param ConsoleLogger $logger
     * @return ContextInterface
     */
    public function setConsoleLogger(ConsoleLogger $logger): ContextInterface
    {
       $this->consoleLogger = $logger;

       return $this;
    }

    /**
     * @return null|string
     */
    public function getConsoleCommand(): ?string
    {
        return $this->consoleCommand;
    }

    /**
     * @param string $command
     * @return ContextInterface
     */
    public function setConsoleCommand(string $command): ContextInterface
    {
        $this->consoleCommand = $command;

        return $this;
    }

    /**
     * @return ContextInterface
     */
    public function clear(): ContextInterface
    {
        return $this;
    }
}
