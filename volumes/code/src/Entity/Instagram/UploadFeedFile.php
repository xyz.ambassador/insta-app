<?php

namespace App\Entity\Instagram;

use App\Entity\File;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Instagram\UploadFeedFileRepository")
 * @ORM\Table(name="instagram_upload_feed_file")
 */
class UploadFeedFile
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Instagram\UploadFeed", inversedBy="uploadFeedFiles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $uploadFeed;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\File")
     * @ORM\JoinColumn(nullable=false)
     */
    private $file;

    /**
     * @ORM\Column(type="smallint")
     */
    private $priority;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUploadFeed(): ?UploadFeed
    {
        return $this->uploadFeed;
    }

    public function setUploadFeed(?UploadFeed $uploadFeed): self
    {
        $this->uploadFeed = $uploadFeed;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }
}
