<?php declare(strict_types = 1);

namespace App\Context\Declaration;

use App\Entity\Instagram\Account;
use Symfony\Component\Console\Logger\ConsoleLogger;

/**
 * Interface ContextInterface
 * @package App\Context\Declaration
 */
interface ContextInterface
{
    /**
     * @return Account
     */
    public function getAccount(): ?Account;

    /**
     * @param Account $account
     *
     * @return ContextInterface
     */
    public function setAccount(Account $account): self;

    /**
     * @return null|ConsoleLogger
     */
    public function getConsoleLogger(): ?ConsoleLogger;

    /**
     * @param ConsoleLogger $logger
     * @return ContextInterface
     */
    public function setConsoleLogger(ConsoleLogger $logger): self;

    /**
     * @return null|string
     */
    public function getConsoleCommand(): ?string;

    /**
     * @param string $command
     * @return ContextInterface
     */
    public function setConsoleCommand(string $command): self;

    /**
     * @return ContextInterface
     */
    public function clear(): self;
}
